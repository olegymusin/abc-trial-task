import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './navmenu/navmenu.component';
import { PageNotFoundComponent } from './page-not-found.component';

import { ManagersModule } from './managers/managers.module';
import { OrdersModule } from './orders/orders.module';
import { AppRoutingModule } from './app-routing.module';
import { AddOrderComponent } from './orders/add-order/add-order.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    AddOrderComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ManagersModule,
    OrdersModule,
    AppRoutingModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
