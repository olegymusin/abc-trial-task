import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Manager } from '../manager';
import { MANAGERS } from '../../mock-data';

const managersPromise = Promise.resolve(MANAGERS);

@Injectable()
export class ManagerService {

    getManagers(): Promise<Manager[]> {
        return managersPromise;
    }


    getManagerById(id: number): Promise<Manager> {
        return this.getManagers().then(mp => mp.find(manager => manager.id === id));
    }

}
