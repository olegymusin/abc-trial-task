import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagersRoutingModule } from './managers-routing.module';
import { ManagersComponent } from './managers.component';
import { ManagersListComponent } from './managers-list/managers.list.component';
import { ManagerDetailsComponent } from './managers-list/manager-details/manager.details.component';
import { ManagerService } from './service/manager.service';

@NgModule({
    declarations: [
        ManagersComponent,
        ManagersListComponent,
        ManagerDetailsComponent
    ],
    imports: [CommonModule, ManagersRoutingModule],
    exports: [],
    providers: [ManagerService],
})
export class ManagersModule { }
