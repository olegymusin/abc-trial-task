import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ManagerService } from '../service/manager.service';
import { Manager } from '../manager';
import { OrderService } from '../../orders/service/order.service';

@Component({
  selector: 'managers-list',
  templateUrl: './managers.list.component.html',
  styleUrls: ['./managers.list.component.css']
})
export class ManagersListComponent implements OnInit {

  managers: Promise<Manager[]>;

  constructor(private _managerService: ManagerService) { }

  ngOnInit() {
    this.managers = this._managerService.getManagers();

  }

}
