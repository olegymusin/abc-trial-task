import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { ManagerService } from '../../service/manager.service';
import { Manager } from '../../manager';
import { Order } from '../../../orders/order';
import { OrderService } from '../../../orders/service/order.service';

@Component({
  templateUrl: './manager.details.component.html',
  styleUrls: ['./manager.details.component.css']
})
export class ManagerDetailsComponent implements OnInit {

  @Input() manager: Manager;
  orders: Promise<Order[]>;

  constructor(
    private _managerService: ManagerService,
    private _orderService: OrderService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this._managerService.getManagerById(+params['id']))
      .subscribe(manager => {
        this.manager = manager;
        this.orders = this._orderService.getOrdersByManagerId(manager.id);
      });

  }
}

