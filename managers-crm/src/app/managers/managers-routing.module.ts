
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagersComponent } from './managers.component';
import { ManagersListComponent } from './managers-list/managers.list.component';
import { ManagerDetailsComponent } from './managers-list/manager-details/manager.details.component';


const MANAGER_ROUTES: Routes = [
    {
        path: 'managers',
        component: ManagersComponent,

        children: [
            {
                path: 'list',
                component: ManagersListComponent,
                children: [
                    {
                        path: 'view/:id',
                        component: ManagerDetailsComponent,
                    }
                ]
            },

            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(MANAGER_ROUTES)],
    exports: [RouterModule]
})
export class ManagersRoutingModule { }
