import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';
import { OrdersListComponent } from './orders-list/orders.list.component';

const ORDERS_ROUTES: Routes = [
    {
        path: 'orders',
        component: OrdersComponent,
        children: [
            {
                path: 'list',
                component: OrdersListComponent,
            },
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
   
    imports: [RouterModule.forChild(ORDERS_ROUTES)],
    exports: [RouterModule],
})
export class OrdersRoutingModule { }