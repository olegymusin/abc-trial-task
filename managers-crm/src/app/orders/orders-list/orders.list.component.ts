import { Component, OnInit } from '@angular/core';
import { Order } from '../order';
import { OrderService } from '../service/order.service';

@Component({
    selector: 'orders-list',
    templateUrl: './orders.list.component.html',
    styleUrls: ['./orders.list.component.css']
})
export class OrdersListComponent implements OnInit {

    orders: Promise<Order[]>;
    direction: number;
    isDesc: boolean;
    column: string;

    constructor(private _orderService: OrderService) { }

    ngOnInit() {
        this.orders = this._orderService.getOrders();
    }

    delete(order: Order) {
        this.orders.then(op => op.splice(op.indexOf(order), 1));
    }

    sort(property) {

        this.isDesc = !this.isDesc;
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    }
}