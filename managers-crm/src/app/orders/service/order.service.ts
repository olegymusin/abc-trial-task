import { Injectable } from '@angular/core';
import { ORDERS } from '../../mock-data';
import { Order } from '../order';

const ordersPromise = Promise.resolve(ORDERS);

@Injectable()
export class OrderService {
    getOrders(): Promise<Order[]> {
        return ordersPromise;
    }

    getOrdersByManagerId(id: number): Promise<Order[]> {
        return this.getOrders().then(op => op.filter(order => order.managerId === id));
    }

    addOrder(order: Order): Promise<Order> {
        return this.getOrders()
            .then(orders => {
                const maxIndex = orders.length - 1;
                const ordersWithMaxIndex = orders[maxIndex];
                order.id = ordersWithMaxIndex.id + 1;
                orders.push(order);
                return order;
            }
            );
    }

}
