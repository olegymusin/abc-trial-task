import { Component, OnInit } from '@angular/core';
import { Order } from '../order';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../service/order.service';
import { Manager } from '../../managers/manager';
import { ManagerService } from '../../managers/service/manager.service';

@Component({
    selector: 'add-order',
    templateUrl: './add-order.component.html'
})
export class AddOrderComponent implements OnInit {

    order: Order;
    manager: Manager;

    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _orderService: OrderService,
        private _managerService: ManagerService) {

    }

    ngOnInit() {
        
        this.manager = this._route.snapshot.data['currentUser'];
        this.order = new Order(null, null, null, this.manager.id);
    }

    onSubmit() {
        this._orderService.addOrder(this.order)
            .then(data =>
                this._router.navigate(['../orders/list/'], { relativeTo: this._route }));
    }

}
