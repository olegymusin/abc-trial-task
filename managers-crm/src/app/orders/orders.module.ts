import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import { OrderService } from './service/order.service';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersListComponent } from './orders-list/orders.list.component';
import { OrderByPipe } from './pipes/orderby.pipe';

@NgModule({
    declarations: [
        OrdersComponent,
        OrdersListComponent,
        OrderByPipe],
    imports: [ CommonModule, OrdersRoutingModule ],
    exports: [],
    providers: [OrderService],
})
export class OrdersModule {}
