export class Order {
    constructor(public id: number, public product: string,
        public qty: number, public managerId: number) {

    }
}
