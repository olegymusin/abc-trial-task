import { Manager } from './managers/manager';
import { Order } from './orders/order';


export const MANAGERS: Manager[] = [
    new Manager(1, 'Jonh D'),
    new Manager(2, 'Sivester Daw'),
    new Manager(3, 'Alister McCain'),
    new Manager(4, 'Bertran Rassel'),
    new Manager(5, 'Vernor Vindge'),

];

export const ORDERS: Order[] = [
    new Order(1, 'Boots', 1200, 2),
    new Order(2, 'Shirts', 150, 1),
    new Order(3, 'Shirts', 1000, 5),
    new Order(4, 'Scarf', 75, 5),
    new Order(5, 'Socks', 2000, 4),
    new Order(6, 'Caps', 650, 4),
    new Order(7, 'Glasses', 400, 3),
    new Order(8, 'Caps', 150, 3),
    new Order(9, 'Suits', 300, 4),
]
