import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    template: `<div class='row'>
                    <div class='col-md-6'>
                        <h2>Oops! Page Not Found.</h2>
                        <h4>Press the button to return to previous page.</h4>
                        <button class='btn btn-info' (click)="goBack()">Go Back</button>
                    </div>
                </div>`
})
export class PageNotFoundComponent {
    constructor(private location: Location) { }
    goBack(): void {
        this.location.back();
    }
}
