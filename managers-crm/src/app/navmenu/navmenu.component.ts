import { Component, ViewEncapsulation } from '@angular/core';
import { Manager } from '../managers/manager';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';


@Component({
    moduleId: module.id,
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class NavMenuComponent {
   
}
