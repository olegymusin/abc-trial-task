import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';
import { Manager } from './managers/manager';
import { MANAGERS } from './mock-data';

const currentUser: Manager = MANAGERS[3]; // Например, мы авторизовались под 4-ым манагером
const APP_ROUTES: Routes = [
    {
        path: 'managers',
        loadChildren: 'app/managers/managers.module#ManagersModule',
        data: { currentUser: currentUser }

    },
    {
        path: 'orders',
        loadChildren: 'app/orders/orders.module#OrdersModule',
        data: { currentUser: currentUser }
    },
    {
        path: 'new-order',
        component: AddOrderComponent,
        data: { currentUser: currentUser }
    },
    {
        path: '',
        redirectTo: 'managers',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];


@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(APP_ROUTES)],
    exports: [RouterModule],
    providers: [],
})
export class AppRoutingModule { }
